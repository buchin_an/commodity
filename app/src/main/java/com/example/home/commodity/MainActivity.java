package com.example.home.commodity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.home.commodity.adapter.ShopAdapter;
import com.example.home.commodity.model.Product;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.product_list)
    RecyclerView productList;
    ShopAdapter adapter;
    Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        adapter = new ShopAdapter(this);

        productList.setAdapter(adapter);
        productList.setLayoutManager(new LinearLayoutManager(this));


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (Shop.isWorking()) {
                    if (!PurchaseRequest.isRequestsEmpty() && !Shop.isShopEmpty()) {
                        removeProduct();
                        adapter.notifyDataSetChanged();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        };

        goodsDeliver().start();
        requestListener().start();
        stopShop().start();
        buyProduct(1000).start();

    }

    private void removeProduct() {
        PurchaseRequest.removeRequest();
        Shop.removeProduct();
    }


    public Thread stopShop() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Shop.setWorking(!Shop.isWorking());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public Thread requestListener() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (!PurchaseRequest.isRequestsEmpty()) {
                        handler.sendEmptyMessage(1);
                    }
                }
            }
        });
    }

    public Thread goodsDeliver() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (Shop.isShopEmpty()) {
                        for (int i = 0; i < 5; i++) {
                            Shop.addProduct(new Product("name" + i, i));
                            handler.sendEmptyMessage(2);
                        }
                    }
                }

            }
        });
    }

    public Thread buyProduct(final int delay) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    PurchaseRequest.addRequest("buy");
                    try {
                        Thread.sleep(delay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        });
    }


}

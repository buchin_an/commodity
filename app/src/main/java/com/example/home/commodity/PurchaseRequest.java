package com.example.home.commodity;

import java.util.LinkedList;

public class PurchaseRequest {

    static LinkedList<String> requests = new LinkedList<>();

    public static synchronized void addRequest(String request) {
        requests.add(request);
    }

    public static void removeRequest() {
        if (!requests.isEmpty()) {
            requests.removeFirst();
        }
    }

    public static synchronized boolean isRequestsEmpty() {
        return requests.isEmpty();
    }

}

package com.example.home.commodity;

import com.example.home.commodity.model.Product;

import java.util.LinkedList;
import java.util.List;

public class Shop {

    private static LinkedList<Product> products = new LinkedList<>();
    private static Boolean working = false;


    public static synchronized void addProduct(Product product) {
        products.addLast(product);
    }

    public static void removeProduct() {

        if (!products.isEmpty()) {
            products.removeFirst();
        }

    }

    public static List<Product> getAllProduct() {
        return products;
    }

    public static boolean isShopEmpty() {
        return products.isEmpty();
    }

    public static synchronized boolean isWorking() {
        return working;
    }

    public static synchronized void setWorking(boolean isWorking) {
        Shop.working = isWorking;
    }
}
